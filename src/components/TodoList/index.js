import React, { Component } from 'react';
import { List } from './styles';
import TodoItem from '../TodoItem';

class TodoList extends Component {


  render() {
    const { todos } = this.props;
    return (
      <List>
        {
          todos.map(todo => <TodoItem key={todo.id} todo={todo} />)
        }
      </List>
    );
  }
}

export default TodoList;