import React, { Component } from 'react';
import { ListItem, Status, TodoTitle } from './styles';
import { TodoContext } from '../Context';

class TodoItem extends Component {
  render() {
    const { completed, title, id } = this.props.todo;
    return (
      <TodoContext.Consumer>
        {
          ({ changeState, removeItem }) => (
            <ListItem >
              <Status onClick={() => changeState(id)} completed={completed} />
              <TodoTitle onClick={() => removeItem(id)} completed={completed}>{title}</TodoTitle>
            </ListItem>
          )
        }
      </TodoContext.Consumer>
    )
  }
}

export default TodoItem;