import styled from 'styled-components';

export const ListItem = styled.li`
  padding:  2rem 1.5rem;
  background: #23232d; 
  display: flex;
  flex-direction: row;

  & + li  {
    margin-top: 0.2rem;
  }
`;

export const Status = styled.span`
  width: 20px;
  height: 20px;
  margin-right: 1rem;
  border-radius: 50%;
  border: 1px solid #aaa;
  display: inline-block;
  background: ${props => props.completed ? 'cyan' : ''};
`;

export const TodoTitle = styled.span`
  flex: 1;
  color: ${props => props.completed ? 'cyan' : '#aaa'};
  text-decoration: ${props => props.completed ? 'line-through' : 'none'};
`;