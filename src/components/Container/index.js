import styled from 'styled-components'

export const Container = styled.div`
  width: 85%;
  height: 100vh;
  margin: 3rem auto;
`;

export default Container;
