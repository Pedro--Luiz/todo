import React from 'react';

export const TodoContext = React.createContext({
  changeState: state => { },
  removeItem: itemId => { },
})