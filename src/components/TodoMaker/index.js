import React, { Component } from 'react';
import { TodoName, TodoCreate } from './styles';

class TodoMaker extends Component {
  state = {
    newItem: '',
  }

  handleInputChange = event => {
    this.setState({ newItem: event.target.value });
  }

  handleAdd = () => {
    const { onAdd } = this.props;
    onAdd(this.state.newItem);
    this.setState({ newItem: '' });
  }

  render() {
    return (
      <>
        <TodoName onChange={this.handleInputChange} value={this.state.newItem} />
        <TodoCreate onClick={this.handleAdd}>+</TodoCreate>
      </>
    );
  }
}

export default TodoMaker;