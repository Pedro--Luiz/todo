import styled from 'styled-components';

export const TodoName = styled.input.attrs({
  type: 'text',
})`
  width: 100%;
  margin-bottom: 1rem;
  font-size: 1.8rem;
  color: #aaa;
  background: none;
  border-width: 0 0 1px 0;
  outline: none;
  display: block;
`;

export const TodoCreate = styled.button`
  width: 100%;
  margin-bottom: 1rem;
  font-size: 1.8rem;
  color: #aaa;
  background: none;
  border: none;
  outline: none;
  display: block;
`;