import React, { Component } from 'react';
import Container from '../../components/Container';
import TodoList from '../../components/TodoList';
import TodoMaker from '../../components/TodoMaker';
import { TodoContext } from '../../components/Context';

class Home extends Component {

  state = {
    todos: [
      {
        "id": 1,
        "title": "delectus aut autem",
        "completed": false
      },
      {
        "id": 2,
        "title": "quis ut nam facilis et officia qui",
        "completed": false
      },
      {
        "id": 3,
        "title": "fugiat veniam minus",
        "completed": false
      },
      {
        "userId": 1,
        "id": 4,
        "title": "et porro tempora",
        "completed": true
      },
      {
        "id": 5,
        "title": "laboriosam mollitia et enim quasi adipisci quia provident illum",
        "completed": false
      },
      {
        "id": 6,
        "title": "qui ullam ratione quibusdam voluptatem quia omnis",
        "completed": false
      },
    ],
  }

  addItem = item => {
    const newItem = {
      id: Math.ceil(Math.random() * 8000),
      title: item,
      completed: false,
    };
    this.setState({ todos: [newItem, ...this.state.todos] })
  }

  removeItem = itemId => {
    const { todos } = this.state;
    this.setState({ todos: todos.filter(todo => todo.id !== itemId) });
  }

  changeState = itemId => {
    const { todos } = this.state;
    const updatedTodos = todos.map(todo => {
      if (todo.id === itemId) {
        todo.completed = !todo.completed;
      }
      return todo;
    });
    this.setState({ todos: updatedTodos });
  }

  render() {
    const { todos } = this.state;
    const { changeState, removeItem } = this;
    const provideMethods = {
      changeState,
      removeItem,
    }
    return (
      <Container>
        <TodoMaker onAdd={this.addItem} />
        <TodoContext.Provider value={provideMethods}>
          <TodoList todos={todos} />
        </TodoContext.Provider>
      </Container>
    );
  }
}

export default Home;